/**
 * @file
 * Provide summary information inside vertical tabs with jQuery.
 */

(function ($) {

/**
 * Provide summary information for vertical tabs.
 */
Drupal.behaviors.uw_auth_node_edit = {
  attach: function (context) {

    // Provide summary when editing a node.
    $('fieldset#edit-uw-auth-node-edit', context).drupalSetSummary(function (context) {
      var vals = [];
      if ($('#edit-uw-auth-node-enable:checked', context).is(':checked')) {
        vals.push(Drupal.t('Enabled'));
      }
      else {
        vals.push(Drupal.t('Disabled'));
      }
      return vals.join('<br/>');
    });

    // Provide summary during content type configuration.
    $('fieldset#edit-uw-auth-node', context).drupalSetSummary(function (context) {
      var vals = [];
      if ($('#edit-uw-auth-node-node-type', context).is(':checked')) {
        vals.push(Drupal.t('Enabled'));
      }
      else {
        vals.push(Drupal.t('Disabled'));
      }
      return vals.join('<br/>');
    });

  }
};

})(jQuery);
